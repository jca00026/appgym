import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import {AngularFireModule} from '@angular/fire'
import {environment} from '../environments/environment'
import { AppComponent } from './app.component';
import { AngularFirestore } from '@angular/fire/firestore';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DiaComponent } from '../app/componentes/dia/dia.component';
import { PrincipalComponent } from './componentes/principal/principal.component';
import { MenuComponent } from './componentes/menu/menu.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatToolbarModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule, MatSlideToggleModule, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatDialogModule, MatInputModule, MatSelectModule, MatTabsModule, MatCheckboxModule} from  '@angular/material';
import { PesoComponent, DialogMeta, DialogPeso } from './componentes/peso/peso.component';
import { ErrorComponent } from './componentes/error/error.component';
import { ComunicacionService } from './servicio/comunicacion.service';
import { LoginComponent } from './componentes/login/login.component';
import { AppGuard } from './app.guard';
import { RegistroComponent } from './componentes/registro/registro.component';
import { RestablecerContraseniaComponent } from './componentes/restablecer-contrasenia/restablecer-contrasenia.component';
import {AngularFireAuthModule} from '@angular/fire/auth'
import { AuthService } from './servicio/auth.service';
import { HistorialComponent, HeaderComponent} from './componentes/historial/historial.component';
import {MAT_DATE_LOCALE} from '@angular/material';
import { ChartsModule } from 'ng2-charts';
import {MatCardModule} from '@angular/material/card';
import { EjerciciosComponent } from './componentes/ejercicios/ejercicios.component';
import { ShowEjercicioComponent } from './componentes/ejercicios/ventanas/show-ejercicio/show-ejercicio.component';
import { AddEjercicioComponent } from './componentes/ejercicios/ventanas/add-ejercicio/add-ejercicio.component';
import { NuevaRutinaComponent } from './componentes/nueva-rutina/nueva-rutina.component';
import { MisrutinasComponent } from './componentes/misrutinas/misrutinas.component';
import { NuevaComponent } from './componentes/nueva-rutina/modals/nueva/nueva.component';


import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { MyHammerConfig } from './hammer.component';
import { CambiarNombreComponent } from './componentes/nueva-rutina/modals/cambiar-nombre/cambiar-nombre.component';
import { AddEjercicioRutinaComponent } from './componentes/ejercicios/ventanas/add-ejercicio-rutina/add-ejercicio-rutina.component';
import { InfoComponent } from './componentes/nueva-rutina/modals/info/info.component';
import { EliminarComponent } from './componentes/nueva-rutina/modals/eliminar/eliminar.component';
import { EjecutarRutinaComponent } from './componentes/ejecutar-rutina/ejecutar-rutina.component';
import { MuestraHistoriaComponent } from './componentes/muestra-historia/muestra-historia.component';


@NgModule({
  declarations: [
    AppComponent,
    DiaComponent,
    PrincipalComponent,
    MenuComponent,
    PesoComponent,
    ErrorComponent,
    LoginComponent,
    RegistroComponent,
    RestablecerContraseniaComponent,
    HistorialComponent,
    HeaderComponent,
    DialogMeta,
    DialogPeso,
    EjerciciosComponent,
    ShowEjercicioComponent,
    AddEjercicioComponent,
    NuevaRutinaComponent,
    MisrutinasComponent,
    NuevaComponent,
    CambiarNombreComponent,
    AddEjercicioRutinaComponent,
    InfoComponent,
    EliminarComponent,
    EjecutarRutinaComponent,
    MuestraHistoriaComponent
  ],
  imports: [
    MatCardModule,
    MatDialogModule,
    ChartsModule,
    BrowserModule,
    FormsModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    BrowserAnimationsModule,
    NoopAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatSidenavModule,
    MatSelectModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    AngularFireAuthModule,
    MatTabsModule,
    MatCheckboxModule,
    
    
  ],
  entryComponents: [
    HeaderComponent,
    DialogMeta,
    DialogPeso,
    AddEjercicioComponent,
    ShowEjercicioComponent,
    NuevaComponent,
    CambiarNombreComponent,
    AddEjercicioRutinaComponent,
    InfoComponent,
    EliminarComponent,
    MuestraHistoriaComponent],
  providers: [AngularFirestore, ComunicacionService,AppGuard, AuthService, {provide:MAT_DATE_LOCALE, useValue: 'es-ES'}, {
    provide: HAMMER_GESTURE_CONFIG,
    useClass: MyHammerConfig
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }