export class Ejercicio {

   public  nombre:string;
   public musculo:string;
   //private historial:Array<any>;
   public descripcion:string;
   public serie:number;
   public repe:boolean;
   public repeticiones:Array<string>;
    public idEjercicio:string;
    public color:number;
    public imagen;;
    public dif;
    public num;
  

   constructor( nombre:string, musculo:string, descripcion:string, serie:number, repe:boolean, repeticiones:Array<string>,idEjercicio:string,color:number, dif:string, num:number){
       this.nombre=nombre;
       this.musculo=musculo;
       this.descripcion=descripcion;
       this.serie=serie;
       this.repe=repe;
       this.repeticiones=repeticiones;
       this.idEjercicio=idEjercicio;
       this.color=color;
       this.dif=dif;
       this.num=num;
       this.imagen=this.imagenes(musculo);
   }
   getNombre(){
     return this.nombre;
   }
   getMusculo(){
     return this.musculo;
   }
   getNum(){
     return this.num;
   }
   imagenes(tipo){
    switch(tipo){
      case "Abdomen":
        return "assets/imagenes/tipo/abdomen.png";
      case "Biceps":
        return "assets/imagenes/tipo/biceps.png";
      case 'Cinta':
          return "assets/imagenes/tipo/cinta.png";
      case 'Bicicleta':
          return "assets/imagenes/tipo/bici.png";
      case 'Eliptica':
          return "assets/imagenes/tipo/eliptica.png";
      case 'Espalda':
        return "assets/imagenes/tipo/espalda.png";
      case 'Hombro':
        return "assets/imagenes/tipo/hombro2.png";
      case 'Pecho':
        return "assets/imagenes/tipo/pecho2.png";
      case 'Piernas Superiores':
        return "assets/imagenes/tipo/pierna-c.png";
        case 'Piernas Inferiores':
        return "assets/imagenes/tipo/pierna-gemelo.png";
      case 'Oblicuos':
        return "assets/imagenes/tipo/oblicuos.png";
      case 'Triceps':
        return "assets/imagenes/tipo/triceps.png";
    }
  }
} 
