import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalComponent } from './componentes/principal/principal.component';
import { PesoComponent } from './componentes/peso/peso.component';
import { DiaComponent } from './componentes/dia/dia.component';
import { MenuComponent } from './componentes/menu/menu.component';
import { ErrorComponent } from './componentes/error/error.component';
import { LoginComponent } from './componentes/login/login.component';
import { AppGuard } from './app.guard';
import { RegistroComponent } from './componentes/registro/registro.component';
import { RestablecerContraseniaComponent } from './componentes/restablecer-contrasenia/restablecer-contrasenia.component';
import { HistorialComponent } from './componentes/historial/historial.component';
import { EjerciciosComponent } from './componentes/ejercicios/ejercicios.component';
import { MisrutinasComponent } from './componentes/misrutinas/misrutinas.component';
import { NuevaRutinaComponent } from './componentes/nueva-rutina/nueva-rutina.component';
import { EjecutarRutinaComponent } from './componentes/ejecutar-rutina/ejecutar-rutina.component';


const routes: Routes = [
  
  {path:'', component: LoginComponent},
  {path: 'registro', component: RegistroComponent},
  {path: 'forgotpassword', component: RestablecerContraseniaComponent},
  {path: 'menu',canActivate: [AppGuard], component: MenuComponent,
    children:[
      {path:'', canActivate: [AppGuard], component: PrincipalComponent},
      {path: 'principal',canActivate: [AppGuard],component: PrincipalComponent},
      
      {path: 'historial',canActivate:[AppGuard], component: HistorialComponent},
      {path: 'peso',canActivate: [AppGuard],component: PesoComponent},
      {path: 'peso/:add',canActivate: [AppGuard],component: PesoComponent},
      {path: 'ejercicios',canActivate:[AppGuard], component: EjerciciosComponent},
      {path: 'ejercicios/:rutina',canActivate:[AppGuard], component: EjerciciosComponent},
      {path:'misRutinas',canActivate:[AppGuard], component:MisrutinasComponent},
      {path: '**', component: ErrorComponent}
    ]
  }, 
  {path: 'dia/:idDia',canActivate: [AppGuard], component: DiaComponent},
  {path:'nuevaRutina/:id', canActivate:[AppGuard], component: NuevaRutinaComponent},
  {path:'ejecutar',component: EjecutarRutinaComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
