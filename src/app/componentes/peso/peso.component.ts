import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FirestoreService } from 'src/app/Servicio/firestore.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label, Color, BaseChartDirective } from 'ng2-charts';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ViewEncapsulation } from '@angular/core';
import {animate,keyframes,query,stagger,state,style,transition,trigger} from '@angular/animations';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ComunicacionService } from 'src/app/servicio/comunicacion.service';
import { EliminarComponent } from '../nueva-rutina/modals/eliminar/eliminar.component';
//import * as annotations from 'chartjs-plugin-annotation';

export const speedDialFabAnimations = [
  trigger('fabToggler', [
    state('inactive', style({
      transform: 'rotate(0deg)'
    })),
    state('active', style({
      transform: 'rotate(225deg)'
    })),
    transition('* <=> *', animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
  ]),
  trigger('speedDialStagger', [
    transition('* => *', [

      query(':enter', style({ opacity: 0 }), {optional: true}),

      query(':enter', stagger('40ms',
        [
          animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)',
            keyframes(
              [
                style({opacity: 0, transform: 'translateY(10px)'}),
                style({opacity: 1, transform: 'translateY(0)'}),
              ]
            )
          )
        ]
      ), {optional: true}),

      query(':leave',
        animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)',
          keyframes([
            style({opacity: 1}),
            style({opacity: 0}),
          ])
        ), {optional: true}
      )

    ])
  ])
];
export interface DialogData {
  meta: string;
  peso: string;
  nuevo: boolean;
}

@Component({
  selector: 'app-peso',
  templateUrl: './peso.component.html',
  styleUrls: ['./peso.component.css'],
  animations: speedDialFabAnimations
})
export class PesoComponent implements OnInit {

  meta: string;
  peso: string;
  private eli:boolean;
  private contpeso;
  private historialPesos:Map<string,any>;
  private vectPesos=[];
  private user;
  private add:string;
  @ViewChild(BaseChartDirective,{ static: false }) chart: BaseChartDirective
  public variable: any;
  constructor(private router: Router, private firestoreService:FirestoreService,public dialog: MatDialog, private comunicacionService: ComunicacionService, private route: ActivatedRoute) { 
    //BaseChartDirective.registerPlugin(annotations);
    this.historialPesos=new Map();

    this.contpeso=0;
    this.variable=50;
    this.eli=false;
  }

  ngOnInit() {
    this.RecogerParametros();
    this.cambiarTitulo("Peso");
    this.meta="-";
    this.peso="-";
    this.firestoreService.getDataUser().subscribe((usuario)=>{
      this.user={
        datos: usuario.payload.data(),
      };
      if(this.user.datos.Meta==""){
        this.openMeta(true);
        
      }else{
        this.meta=this.user.datos.Meta;
        this.subHistorialPesos();
      }
    });
    if(this.add=="add"){
      this.openPeso();
    }
}
  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'Peso' },
    { data: [], label: 'Meta' }
  ];
  public lineChartLabels: Label[] = [];

  public lineChartColors: Color[] = [
    {
      borderColor: 'mediumblue',
      backgroundColor: 'rgb(55, 55, 230,0.3)',
     
    },
    {
      borderColor: '#8400FF',
      backgroundColor: 'rgb(157, 80, 230,0.3)',
      pointRadius:0,
    },
  ];
  public lineChartOptions= {
    
    elements: {
      point: {
          radius: 4,
          borderWidth: 2,
          hoverRadius:6,
          hoverBorderWidth:3
      }
  },
   scales:
    { 
      
     xAxes: [
       { 
        ticks:{
          maxTicksLimit: 5,
          display: false,
        },
       }
     ],
     yAxes:[
      { 
        ticks:{
          suggestedMin: 60,
        },
      }
    ] 
  },/*
  annotation: {
    annotations: [
      {
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        value: 75,
        borderColor: '#8400FF',
        backgroundColor: 'rgb(157, 80, 230,0.3)',
        borderWidth: 2,
      
      },
    ],
  },
*/
  };
  
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  fabButtons = [
    {
      label: "Meta"
    },
    {
      label: "Peso"
    },
  ];
  buttons = [];
  fabTogglerState = 'inactive';

  subHistorialPesos(){
    this.firestoreService.getHitorialPesos().subscribe((pesos)=>{
      this.historialPesos.clear();
      this.vectPesos=[];
      pesos.forEach((p: any)=>{
        let _fecha= new Date(p.payload.doc.data().Fecha.seconds * 1000);
        let _id=p.payload.doc.id;
        let data={
          id:p.payload.doc.id,
          idHistorial:p.payload.doc.data().IdHistorial,
          peso:p.payload.doc.data().Peso,
          fecha: _fecha,
          elimianr:false,
        }
        this.historialPesos.set(_id, data);
        this.vectPesos.push(data);
      })
      this.vectPesos.sort((n1,n2)=>{
        if(new Date(n1.fecha)<new Date(n2.fecha)) return 1;
        if(new Date(n1.fecha)>new Date(n2.fecha)) return -1;
        return 0;
      });
      this.peso=this.vectPesos[0].peso;
      for(let i of this.vectPesos){
        
        let _fecha= new Date(i.fecha);
        let options = {
          day:"2-digit",
          month: "2-digit",
          year:"numeric",
        };
        let f= _fecha.toLocaleString('es-MX',options);
        i.fecha=f;
      }
      this.dibujar();
    })
  }
  dibujar(){
    this.lineChartData[0].data=[];
    this.lineChartLabels=[];
    this.lineChartData[1].data=[];
    this.lineChartData[1].data.push(parseFloat(this.meta));
    this.lineChartData[1].data.push(parseFloat(this.meta));
    let cont=0;
    let copia =this.vectPesos.slice();
    copia.reverse();
    let f;
   for(let i of copia){
     f=i.fecha;
      cont++;
      if(cont< this.contpeso){
        this.lineChartData[0].data.push(i.peso);
        this.lineChartLabels.push(i.fecha);
      }else{
        this.lineChartData[0].data.push(i.peso);
        this.lineChartLabels.push(i.fecha);
        this.lineChartData[1].data.push(parseInt(this.meta));
      }
      
     
    }
    this.lineChartLabels.push(f);
    this.chart.chart.update();
  
    
  }
  dibujarMeta(){
    if(this.contpeso==0){
      this.lineChartData[1].data.push(parseInt(this.meta));
      this.lineChartData[1].data.push(parseInt(this.meta));
      this.contpeso=2;
    }else{
      for(let i of this.lineChartData[1].data){
        i=parseInt(this.meta);
      }

    }
    let valor=parseInt(this.meta)-10;
    this.chart.chart.options.scales.yAxes[0].ticks.suggestedMin=valor;
    
    this.chart.chart.update();
    this.firestoreService.cambiarMeta(parseInt(this.meta));
  }
  cambiarTitulo(nombre){
    localStorage.setItem("titulo",nombre);
    this.comunicacionService.setTitulo(nombre);
  }
  showItems() {
    this.fabTogglerState = 'active';
    this.buttons = this.fabButtons;
  }
  hideItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }
  onToggleFab() {
    this.buttons.length ? this.hideItems() : this.showItems();
  }
  fabButton(nameButton){
    if(nameButton=="Peso"){
      this.openPeso();
    }else{
      this.openMeta(false);
    }
    this.onToggleFab();
  }
  showPeso(id){
    let ppp=this.historialPesos.get(id);
  }
   RecogerParametros(){
    this.route.params.subscribe((params: Params) =>{
      this.add= params.add;
    })
  }
  eliminarElementos(){
    if(this.eli){
      this.eli=false;
      for(let i=0; i<this.vectPesos.length;i++){
        this.vectPesos[i].eliminar=false;
      }
    }else{
      this.eli=true;
    }
  }
  openPeso(){
    const dialogRef = this.dialog.open(DialogPeso, {
      width: '374px',
      height: '374px', 
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result!=undefined){
        let id1=this.firestoreService.generarID();
        let data={
          id: id1,
          peso:result.peso,
          fecha: new Date(result.fecha),
        }
        this.firestoreService.addHistorialPeso(data);
        this.firestoreService.addActualizacionPeso(data);

      }
    });
  }
  
  openMeta(_nuevo){
    const dialogRef = this.dialog.open(DialogMeta, {
      width: '374px',
      height: '277px',
      data: {meta: this.meta, nuevo:_nuevo},
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result!=undefined){
        this.meta = result;
        this.dibujarMeta();
      }
    });
  }

  mensajeError(){
    const dialogRef = this.dialog.open(EliminarComponent,{
      width: '374px',
      height: 'auto',
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result!=undefined){
        //eliminar de la base de datos
       for(let i=0; i<this.vectPesos.length;i++){
         if(this.vectPesos[i].eliminar==true){    
           this.firestoreService.deleteHistorial(this.vectPesos[i].idHistorial);
           this.firestoreService.deletePeso(this.vectPesos[i].id);
         }
       }
       this.eli=false;
      }
    });
  }
}




@Component({
  selector: 'dialog-meta',
  templateUrl: './ventanas/addMeta.component.html',
  styleUrls: ['./ventanas/addMeta.component.css']
})
export class DialogMeta {
  private nuevo:boolean;
  constructor(
    public dialogRef: MatDialogRef<DialogMeta>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.nuevo=data.nuevo;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'dialog-peso',
  templateUrl: './ventanas/addPeso.component.html',
  styleUrls: ['./ventanas/addPeso.component.css']
})
export class DialogPeso {
  pesoForm: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<DialogPeso>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private formBuilder: FormBuilder) {
      this.pesoForm=this.formBuilder.group({
        peso:['',Validators.required],
        fecha:[new Date().toISOString(),Validators.required],
      });
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
  addPeso(form){
    if(this.comprobarCampos()){
     
      this.dialogRef.close(this.pesoForm.value);
    }
  }
  comprobarCampos():boolean{
    var result: boolean=true;
    var peso = (document.getElementById('peso') as HTMLInputElement);
    var fecha= (document.getElementById('fecha')as HTMLInputElement);
    if(this.pesoForm.get("peso").invalid){
     
      peso.style.setProperty("--c","red");
      result=false;
    }
    if(this.pesoForm.get("fecha").invalid){
      
      fecha.style.setProperty("--c","red");
      result=false;
    }
    return result;
  }
 

}