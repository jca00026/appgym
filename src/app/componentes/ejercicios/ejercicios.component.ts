import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { FirestoreService } from 'src/app/Servicio/firestore.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ComunicacionService } from 'src/app/servicio/comunicacion.service';
import { MatDialog } from '@angular/material';
import { AddEjercicioComponent } from './ventanas/add-ejercicio/add-ejercicio.component';
import { element } from 'protractor';
import { ShowEjercicioComponent } from './ventanas/show-ejercicio/show-ejercicio.component';
import { AddEjercicioRutinaComponent } from './ventanas/add-ejercicio-rutina/add-ejercicio-rutina.component';
import { database } from 'firebase';
import { EliminarComponent } from '../nueva-rutina/modals/eliminar/eliminar.component';
import { MuestraHistoriaComponent } from '../muestra-historia/muestra-historia.component';


interface TypeEjercicios {
  value: string;
  viewValue: string;
}
export interface DIA{
  Titulo:string,
  Ejercicios:any,
  Vector:any,
  Numero: number;
}

export interface AddEjercicio {
  id: string;
  nombre: string;
  musculo: string;
  descripcion: string;
  imagen:string;
  rutina:boolean;
}
@Component({
  selector: 'app-ejercicios',
  templateUrl: './ejercicios.component.html',
  styleUrls: ['./ejercicios.component.css']
})
export class EjerciciosComponent implements OnInit, AfterViewInit {
  typeEjercicios: TypeEjercicios[] = [
    {value: 'todos-0', viewValue: 'Todos'},
    {value: 'abdomen-1', viewValue: 'Abdomen'},
    {value: 'biceps-2', viewValue: 'Biceps'},
    {value: 'cardio-3', viewValue: 'Cardio'},
    {value: 'espalda-4', viewValue: 'Espalda'},
    {value: 'hombros-5', viewValue: 'Hombro'},
    {value: 'pecho-6', viewValue: 'Pecho'},
    {value: 'piernas-7', viewValue: 'Piernas Superiores'},
    {value: 'piernas-8', viewValue: 'Piernas Inferiores'},
    {value: 'oblicuos-9', viewValue: 'Oblicuos'},
    {value: 'triceps-10', viewValue: 'Triceps'},
  ];
  selected = 'todos-0';
  rutina;
  eli:boolean
  private numero:number;
  private ejercicios:Map<string,any>
  private vecEjercicios=[];
  constructor(private firestoreService: FirestoreService, private router: Router, private comunicacionService: ComunicacionService,public dialog: MatDialog, private route:ActivatedRoute) { 
    this.ejercicios=new Map();
    this.eli=false;
  }
  ngAfterViewInit(){
    var cambio=(document.getElementsByClassName('mat-select-value'));
    var elemento;
    for(var i=0;i<1;i++){
      elemento=cambio[i];
    }
    (elemento as HTMLSelectElement).style.color="rgba(0,0,0,.87)";
  }

  ngOnInit() {
    this.cambiarTitulo("Ejercicios");
    this.RecogerParametros();
    this.subEjercicios();
  }
  RecogerParametros(){
    this.route.params.subscribe((params: Params) =>{
      this.rutina= params.rutina;
      if(this.rutina=="1"){
        this.firestoreService.getRutinaDia(this.comunicacionService.getIdDiaRutina(),this.comunicacionService.getidRutina()).subscribe( (d)=>{
          this.numero=d.payload.data().Numero;
        });
      }
    })

  }
  subEjercicios(){
    this.firestoreService.getEjercicios().subscribe((ejers)=>{
      this.ejercicios.clear();
      this.vecEjercicios=[];
      ejers.forEach((ejercicio:any)=>{
        let _id=ejercicio.payload.doc.id;
        let datos={
          id: _id,
          nombre:ejercicio.payload.doc.data().Nombre,
          musculo: ejercicio.payload.doc.data().Tipo,
          descripcion:ejercicio.payload.doc.data().Description,
          historial:ejercicio.payload.doc.data().Hist,
          imagen:this.imagenes(ejercicio.payload.doc.data().Tipo),
          ocultar:ejercicio.payload.doc.data().Ocultar,
          eliminar:false,
        }
        if(datos.ocultar==false){
          this.ejercicios.set(_id as string, datos);
        }
      })
      for(let i of this.ejercicios.values()){
        this.vecEjercicios.push(i);
      }
    });
    }
    imagenes(tipo){
      switch(tipo){
        case "Abdomen":
          return "assets/imagenes/tipo/abdomen.png";
        case "Biceps":
          return "assets/imagenes/tipo/biceps.png";
        case 'Cinta':
            return "assets/imagenes/tipo/cinta.png";
        case 'Bicicleta':
            return "assets/imagenes/tipo/bici.png";
        case 'Eliptica':
            return "assets/imagenes/tipo/eliptica.png";
        case 'Espalda':
          return "assets/imagenes/tipo/espalda.png";
        case 'Hombro':
          return "assets/imagenes/tipo/hombro2.png";
        case 'Pecho':
          return "assets/imagenes/tipo/pecho2.png";
        case 'Piernas Superiores':
          return "assets/imagenes/tipo/pierna-c.png";
          case 'Piernas Inferiores':
          return "assets/imagenes/tipo/pierna-gemelo.png";
        case 'Oblicuos':
          return "assets/imagenes/tipo/oblicuos.png";
        case 'Triceps':
          return "assets/imagenes/tipo/triceps.png";
      }

    }
  
  cambiarTitulo(nombre){
    localStorage.setItem("titulo",nombre);
    this.comunicacionService.setTitulo(nombre);
  }
  filtro(type){
    this.vecEjercicios=[]
    if(type=="Todos"){
      for(let i of this.ejercicios.values()){
          this.vecEjercicios.push(i);
      }
    }else{
      for(let i of this.ejercicios.values()){
        if(type=="Cardio"){
          if(i.musculo=="Bicicleta" || i.musculo=="Cinta" || i.musculo=="Eliptica"){
            this.vecEjercicios.push(i);
          }
        }
        if(i.musculo==type){
          this.vecEjercicios.push(i);
        }
      }
    }
    
  }

  addEjercicioVentana(){
    const dialogRef = this.dialog.open(AddEjercicioComponent, {
      width: '374px',
      height: 'auto', 
    });

    dialogRef.afterClosed().subscribe(result => {
      var cambio=(document.getElementsByClassName('mat-select-value'));
      var elemento;
      for(var i=0;i<1;i++){
        elemento=cambio[i];
      }
      (elemento as HTMLSelectElement).style.color="rgba(0,0,0,.87)";
      var cambio1=(document.getElementsByClassName('mat-select-arrow'));
      var elemento1;
      for(var i=0;i<1;i++){
        elemento1=cambio1[i];
      }
      (elemento1 as HTMLSelectElement).style.color="rgba(0,0,0,.87)";
      
      if(result!=undefined){
        let data={
          nombre:result.nombre,
          musculo:result.musculo,
          descripcion:result.descripcion,
        }
        let _id=this.firestoreService.addEjercicios(data);
      }
    });
  }
  mostrar(_id){
    if(this.rutina=="1"){
      this.addEjercicioRutina(_id);
    }else{
      this.showEjercicio(_id);
    }
  }
  addEjercicioRutina(_id){
    const dialofgRef=this.dialog.open(AddEjercicioRutinaComponent,{
      width:'374px',
      height: 'auto',
      data: {id: this.ejercicios.get(_id).id,nombre:this.ejercicios.get(_id).nombre,imagen:this.ejercicios.get(_id).imagen, rutina:false},
    });
    dialofgRef.afterClosed().subscribe(result=>{
      if(result!=undefined){
        let refe=this.firestoreService.getRefEjercicio(_id);
        let _d=this.firestoreService.generarID();
        let data={
         y:{
          IdEjercicio:_id,
          Repe:result.repe,
          Repeticiones:result.repeticiones,
          Referencia:refe,
          Serie:result.serie,
          Dif:_d,
          Num:this.numero,
         },
        }
        let x=this.firestoreService.generarID();
        let dd="0-"+x;
        this.firestoreService.addEjerDia(this.comunicacionService.getidRutina(), this.comunicacionService.getIdDiaRutina(),data,dd);
        this.router.navigate(["nuevaRutina","1"]);
      }
      
    })
  }
  showEjercicio(_id){
    const dialogRef = this.dialog.open(ShowEjercicioComponent, {
      width: '374px',
      height: 'auto', 
      data: {id: this.ejercicios.get(_id).id,nombre:this.ejercicios.get(_id).nombre, musculo:this.ejercicios.get(_id).musculo,descripcion:this.ejercicios.get(_id).descripcion, imagen:this.ejercicios.get(_id).imagen},
    });

    dialogRef.afterClosed().subscribe(result => {
     
      var cambio=(document.getElementsByClassName('mat-select-value'));
      var elemento;
      for(var i=0;i<1;i++){
        elemento=cambio[i];
      }
      (elemento as HTMLSelectElement).style.color="rgba(0,0,0,.87)";
      
     
      if(result!=undefined){
        if(result=="true"){
          this.firestoreService.ocultarEjercicio(_id);
        }else if(result=="histori"){
          console.log("histori");
          this.histori(_id);
        }else{
        let data={
          Nombre:result.nombre,
          Tipo:result.musculo,
          Description:result.descripcion,
        }
        this.firestoreService.setEjercicios(_id,data);
        }
      }
    });
  }
  histori(i) {
    const dialogRef = this.dialog.open(MuestraHistoriaComponent, {
      width: '400px',
      height: '450px',
      data:{id:this.ejercicios.get(i).id}
    });
    dialogRef.afterClosed().subscribe(result => {

    })

  }
  eliminarElementos(){
    if(this.eli){
      this.eli=false;
      for(let i of this.ejercicios.values()){
        i.eliminar=false;
      }
    }else{
      this.eli=true;
    }
  }
  mensajeError(){
    const dialogRef = this.dialog.open(EliminarComponent,{
      width: '374px',
      height: 'auto',
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result!=undefined){
        //eliminar de la base de datos
       for(let i of this.ejercicios.values()){
         if(i.eliminar==true){
           this.firestoreService.ocultarEjercicio(i.id);
         }
       }
       this.eli=false;
       this.ejercicios.clear();
      }
    });
  }
} 
