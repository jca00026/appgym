import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddEjercicio } from '../../ejercicios.component';

@Component({
  selector: 'app-add-ejercicio-rutina',
  templateUrl: './add-ejercicio-rutina.component.html',
  styleUrls: ['./add-ejercicio-rutina.component.css']
})
export class AddEjercicioRutinaComponent implements OnInit {

  private selected;
  private isChecked;
  private modificable:boolean;
  private click:boolean;
  private imagen:string;
  private rutina:boolean;
  private repe:Array<string>;
  private serie=null;
  private repeticiones: Array<string>;
  private sinRepetir:string;
  private idRepeticiones:Array<string>;

  constructor(public dialogRef: MatDialogRef<AddEjercicio>,
    @Inject(MAT_DIALOG_DATA) public data: AddEjercicio) { 
      this.imagen=this.data.imagen;
      this.modificable=true;
      this.click=false;
      this.rutina=data.rutina;
      this.repe=new Array<string>();
      this.repeticiones=new Array<string>();
      this.idRepeticiones= new Array<string>(); 
    this.sinRepetir=null;    }

  ngOnInit() {
    if(this.rutina=true){
    }
    this.disenio();    
    let elemento= (document.getElementById("quitar") as HTMLInputElement)
    elemento.style.setProperty("border-radius","0");
  }
 
  private disenio(){
    var eliminar=(document.getElementById('eliminar')as HTMLInputElement);
    eliminar.style.setProperty("width","36px");
    eliminar.style.setProperty("height","36px");
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

  addSerie(){
    this.serie +=1;
    let nombre=""+this.serie;
    this.repe.push(nombre);
    this.repeticiones.push(null);
    this.idRepeticiones.push("repe"+(this.serie-1));
  }
  substractSerie(){
    this.serie-=1;
    this.repe.pop();
    if(this.serie==0){
      this.serie=null;
    }
    this.repeticiones.pop();
  }

  addEjerDia(){
      if(this.comprobarCampos()){
        if(this.isChecked){
          let documento=(document.getElementById("sinRepetir") as HTMLInputElement);
          let data={
            serie:this.serie,
            repe: true,
            repeticiones:[],
          }
          data.repeticiones.push(documento.value);
          this.dialogRef.close(data);
        }else{
          let r=[];
          for(let i=0;i<this.repeticiones.length;i++){
            let documento=(document.getElementById(this.idRepeticiones[i]) as HTMLInputElement);
            r.push(documento.value);
          }
          let data={
            serie:this.serie,
            repe:false,
            repeticiones:r
          }
          this.dialogRef.close(data);
        }
      }
  }
  comprobarCampos():boolean{
    var result: boolean
    result=true;
    var serie = (document.getElementById('serie') as HTMLInputElement);
    if(this.serie==null){
      serie.style.setProperty("--c","red");
      result=false;
    }else{
      if(this.isChecked){
        //repetir
        let documento=(document.getElementById("sinRepetir") as HTMLInputElement);
        if(documento.value==""){
          var repe = (document.getElementById('sinRepetir') as HTMLInputElement);
          repe.style.setProperty("--c","red");
          result=false;
        }
      }else{
        for(let i=0; i<this.repeticiones.length;i++){
          let id="repe"+i;
          let documento=(document.getElementById(id) as HTMLInputElement);
          if(documento.value==""){
            var repe = (document.getElementById(id) as HTMLInputElement);
            repe.style.setProperty("--c","red");
            result=false;
          }
        }
      }
    }
    
    return result;
  }
  deleteEjercicio(){
    this.dialogRef.close("true");
  }

}
