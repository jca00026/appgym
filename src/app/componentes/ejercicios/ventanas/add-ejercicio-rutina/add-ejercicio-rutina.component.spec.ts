import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEjercicioRutinaComponent } from './add-ejercicio-rutina.component';

describe('AddEjercicioRutinaComponent', () => {
  let component: AddEjercicioRutinaComponent;
  let fixture: ComponentFixture<AddEjercicioRutinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEjercicioRutinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEjercicioRutinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
