import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from 'src/app/componentes/peso/peso.component';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AddEjercicio } from '../../ejercicios.component';



@Component({
  selector: 'app-add-ejercicio',
  templateUrl: './add-ejercicio.component.html',
  styleUrls: ['./add-ejercicio.component.css']
})
export class AddEjercicioComponent implements OnInit {
  selected;
  selected2;
  ejercicioForm: FormGroup;
  private imagen: string;
  constructor(public dialogRef: MatDialogRef<AddEjercicio>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private formBuilder: FormBuilder) { 
      this.ejercicioForm=this.formBuilder.group({
        nombre:['',Validators.required],
        musculo:['',Validators.required],
        descripcion:['']
      });
      this.imagen="assets/imagenes/mancuerna4.png";
    }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  addEjercicio(form){
    if(this.comprobarCampos()){
        this.dialogRef.close(this.ejercicioForm.value);
    }
  }
  comprobarCampos():boolean{
    var result: boolean=true;
    var nombre = (document.getElementById('nombre') as HTMLInputElement);
    var musculo= (document.getElementById('musculo')as HTMLInputElement);
    if(this.ejercicioForm.get("nombre").invalid && this.ejercicioForm.get("musculo").invalid){
     
      nombre.style.setProperty("--c","red");
      result=false;
      musculo.style.setProperty("--p","red");
      result=false;
    }else if(this.ejercicioForm.get("nombre").invalid){
      nombre.style.setProperty("--c","red");
      result=false;
    }else if( this.ejercicioForm.get("musculo").invalid){
      musculo.style.setProperty("--p","red");
      result=false;
    }
    return result;
  }

  cambioimagen(tipo){
    this.imagen=this.imagenes(tipo);
  }
  imagenes(tipo):string{
    switch(tipo){
      case "Abdomen":
        return "assets/imagenes/tipo/abdomen.png";
      case "Biceps":
        return "assets/imagenes/tipo/biceps.png";
      case "Cardio":
        return "assets/imagenes/mancuerna4.png";
      case 'Cinta':
        return "assets/imagenes/tipo/cinta.png";
      case 'Bicicleta':
        return "assets/imagenes/tipo/bici.png";
      case 'Eliptica':
        return "assets/imagenes/tipo/eliptica.png";
      case 'Espalda':
        return "assets/imagenes/tipo/espalda.png";
      case 'Hombro':
        return "assets/imagenes/tipo/hombro2.png";
      case 'Pecho':
        return "assets/imagenes/tipo/pecho2.png";
      case 'Piernas Superiores':
        return "assets/imagenes/tipo/pierna-c.png";
        case 'Piernas Inferiores':
        return "assets/imagenes/tipo/pierna-gemelo.png";
      case 'Oblicuos':
        return "assets/imagenes/tipo/oblicuos.png";
      case 'Triceps':
        return "assets/imagenes/tipo/triceps.png";
    }

  }
}


