import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, throwMatDialogContentAlreadyAttachedError } from '@angular/material';
import { AddEjercicio } from '../../ejercicios.component';
import { DialogData } from 'src/app/componentes/peso/peso.component';
import { componentFactoryName } from '@angular/compiler';

@Component({
  selector: 'app-show-ejercicio',
  templateUrl: './show-ejercicio.component.html',
  styleUrls: ['./show-ejercicio.component.css']
})
export class ShowEjercicioComponent implements OnInit {

  private selected;
  private selected2;
  private modificable:boolean;
  private click:boolean;
  private imagen:string;

  private ejercicioForm: FormGroup;
  constructor(public dialogRef: MatDialogRef<AddEjercicio>,
    @Inject(MAT_DIALOG_DATA) public data: AddEjercicio,private formBuilder: FormBuilder) { 
      this.ejercicioForm = new FormGroup({
        nombre: new FormControl({value:this.data.nombre,disabled:true}, Validators.required),
        musculo: new FormControl({value:this.data.musculo,disabled:true}, Validators.required),
        descripcion: new FormControl({value:this.data.descripcion,disabled:true})
      });
      this.imagen=this.data.imagen;
      this.modificable=false;
      this.click=false;
    }

  ngOnInit() {
    this.selected=this.data.musculo;
    this.disenio();
    
    
    let elemento= (document.getElementById("quitar") as HTMLInputElement)
    elemento.style.setProperty("border-radius","0");
  }
  
  private disenio(){

    var descripcion=(document.getElementById('n3') as HTMLInputElement);
    var eliminar=(document.getElementById('eliminar')as HTMLInputElement);
    eliminar.style.setProperty("width","36px");
    eliminar.style.setProperty("height","36px");
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
 
  cambioimagen(tipo){
    this.imagen=this.imagenes(tipo);
  }
  imagenes(tipo):string{
    switch(tipo){
      case "Abdomen":
        return "assets/imagenes/tipo/abdomen.png";
      case "Biceps":
        return "assets/imagenes/tipo/biceps.png";
      case "Cardio":
        return "assets/imagenes/mancuerna4.png";
      case 'Cinta':
          return "assets/imagenes/tipo/cinta.png";
      case 'Bicicleta':
          return "assets/imagenes/tipo/bici.png";
      case 'Eliptica':
          return "assets/imagenes/tipo/eliptica.png";
      case 'Espalda':
        return "assets/imagenes/tipo/espalda.png";
      case 'Hombro':
        return "assets/imagenes/tipo/hombro2.png";
      case 'Pecho':
        return "assets/imagenes/tipo/pecho2.png";
      case 'Piernas Superiores':
        return "assets/imagenes/tipo/pierna-c.png";
        case 'Piernas Inferiores':
        return "assets/imagenes/tipo/pierna-gemelo.png";
      case 'Oblicuos':
        return "assets/imagenes/tipo/oblicuos.png";
      case 'Triceps':
        return "assets/imagenes/tipo/triceps.png";
    }

  }
  editar(){
    this.modificable=true;
    this.click=true;
    this.ejercicioForm.enable();
   console.log("ksdjfksdf");
  }
  modificar(){
    console.log("sdkfjnksdjfsd");
    if(this.click){
      if(this.comprobarCampos()){
        console.log("111111111111111111111111111111111sdkfjnksdjfsd");
        console.log(this.ejercicioForm.value);
        this.dialogRef.close(this.ejercicioForm.value);
      }

    }
  }
  historial(){
    this.dialogRef.close("histori");
  }
  comprobarCampos():boolean{
    var result: boolean=true;
    var nombre = (document.getElementById('nombre') as HTMLInputElement);
    var musculo= (document.getElementById('musculo')as HTMLInputElement);
    if(this.ejercicioForm.get("nombre").invalid && this.ejercicioForm.get("musculo").invalid){
     
      nombre.style.setProperty("--c","red");
      result=false;
      musculo.style.setProperty("--p","red");
      result=false;
    }else if(this.ejercicioForm.get("nombre").invalid){
      nombre.style.setProperty("--c","red");
      result=false;
    }else if( this.ejercicioForm.get("musculo").invalid){
      musculo.style.setProperty("--p","red");
      result=false;
    }
    return result;
  }
  deleteEjercicio(){
    this.dialogRef.close("true");
  }
}
