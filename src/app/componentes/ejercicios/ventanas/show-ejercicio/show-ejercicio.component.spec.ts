import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowEjercicioComponent } from './show-ejercicio.component';

describe('ShowEjercicioComponent', () => {
  let component: ShowEjercicioComponent;
  let fixture: ComponentFixture<ShowEjercicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowEjercicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowEjercicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
