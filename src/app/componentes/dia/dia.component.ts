import { Component, OnInit, Input, ComponentFactoryResolver } from '@angular/core';
import { Location } from '@angular/common';
import { FirestoreService } from '../../Servicio/firestore.service'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ComunicacionService } from 'src/app/servicio/comunicacion.service';
import { ignoreElements } from 'rxjs/operators';
import { Runtime } from 'inspector';
import { Ejercicio } from 'src/app/clases/ejercicio';


export interface Rutina {
  Nombre: string,
  DiasEntrenar: number,
  DuracionSemanas: number,
}

@Component({
  selector: 'app-dia',
  templateUrl: './dia.component.html',
  styleUrls: ['./dia.component.css']
})
export class DiaComponent implements OnInit {
  private hora: Date;
  private Dia;
  private idDia: string;
  private Superserie: Array<Ejercicio>;
  private vectorEjercicios: Array<Array<number>>;
  private hechos: Array<boolean>;
  private play: boolean;
  private titulo;
  constructor(
    private firestoreService: FirestoreService,
    private route: ActivatedRoute,
    private router: Router,
    private comunicacionService: ComunicacionService,
    private location: Location) {
    // this.Superserie= new Array();
    this.play = false;
    this.hechos = new Array<boolean>();
    this.hora = new Date();
    this.Superserie = new Array<Ejercicio>();
    this.vectorEjercicios = Array<Array<number>>();
  }

  ngOnInit() {
    if (this.play) {
      this.ocultarBoton();
    }
    this.RecogerParametros();
    let idRutina = localStorage.getItem("idRutinaActual");
    this.SubRutinaDiaEjercicio(this.idDia, idRutina);
    this.leerDatos();
  }
  eliminarelementoshechos() {
    this.play = JSON.parse(localStorage.getItem("play"));
    if (this.play) {
      this.hechos = [];
      this.hechos = JSON.parse(localStorage.getItem("hechos"));
      this.hora = new Date(JSON.parse(localStorage.getItem("hora")));
      this.ocultarBoton();
    }
  }
  leerDatos() {
    let datos = localStorage.getItem("hechos");
  }
  public atras() {
    this.router.navigate(["menu"]);
  }
  public ocultarBoton() {
    var elemento = (document.getElementById("atras") as HTMLButtonElement);
    elemento.style.display = "none";
  }
  public RecogerParametros() {
    this.route.params.subscribe((params: Params) => {
      this.idDia = params.idDia;
    })

  }

  public SubRutinaDiaEjercicio(idDia, idRutina) {
    this.Dia = null;
    this.Superserie = [];
    this.hechos = new Array<boolean>();
    this.vectorEjercicios = new Array<Array<number>>();
    let solo = false;
    this.firestoreService.getRutinaDia(idDia, idRutina).subscribe((rutDias) => {
      this.Dia = {
        id: rutDias.payload.id,
        titulo: "",
        data: rutDias.payload.data(),
      };
      //console.log(this.Dia.data);
      this.Dia.titulo = this.Dia.data.Titulo;
      let j = 0;
      for (let ejer of this.Dia.data.Ejercicios) {
        ejer.y.Referencia.get().then(documentSnapshot => {
          if (documentSnapshot.exists) {
            let colorE;
            let codColorE: string;
            codColorE = this.Dia.data.Vector[j];
            codColorE.charAt(0);
            colorE = parseInt(codColorE);
            
            let ejercicio = new Ejercicio(documentSnapshot.data().Nombre, documentSnapshot.data().Tipo, documentSnapshot.data().Description, ejer.y.Serie, ejer.y.Repe, ejer.y.Repeticiones, ejer.y.IdEjercicio, colorE, ejer.y.Dif, ejer.y.Num);
            //console.log(ejercicio);
            //console.log(j);
            this.Superserie.push(ejercicio);
            let funComparation=((n1:Ejercicio,n2:Ejercicio)=>{
              if(n1.num>n2.num) return 1;  
              if(n1.num<n2.num) return -1;
         
            });
            this.Superserie.sort(funComparation);
            console.log(this.Superserie);
           
            j++;
          }
        });
        if (solo != true) {
          let vector = new Array<number>();
          
          let colorActerior;
          for (let i = 0; i < this.Dia.data.Vector.length; i++) {
            let colorE;
            let codColorE: string;
            codColorE = this.Dia.data.Vector[i];
            codColorE.charAt(0);
            colorE = parseInt(codColorE);
            if (i == 0) {
              vector = new Array<number>();
              vector.push(i);

            } else {
              if (colorActerior == colorE) {
                if (colorE == 0) {
                  this.vectorEjercicios.push(vector);
                  this.hechos.push(false);
                  vector = [];
                  vector = new Array<number>();
                  vector.push(i);
                } else {
                  vector.push(i);
                }
              } else {
                this.vectorEjercicios.push(vector);
                this.hechos.push(false);
                vector = [];
                vector = new Array<number>();
                vector.push(i);
              }
            }
            colorActerior = colorE;
            if (i == this.Dia.data.Vector.length - 1) {
              this.vectorEjercicios.push(vector);
              this.hechos.push(false);
            }
          }
          solo=true;
          console.log(this.vectorEjercicios);
        }
      }
      this.eliminarelementoshechos();
    });

  }



  empezar() {
    this.play = true;
    this.hora = new Date();
    this.ocultarBoton();
  }

  terminar() {
    let date = new Date();


    let tiempor = date.getTime() - this.hora.getTime();
    let minutos = tiempor.toLocaleString();
    let h = date.getHours() - this.hora.getHours();
    let m = date.getMinutes() - this.hora.getMinutes();
    let s = date.getSeconds() - this.hora.getSeconds();
    let duracion;

    if (h == 0) {
      duracion = m + "min ";
    } else {
      duracion = h + "h "+m+"min ";
    }
    localStorage.removeItem("hechos");
    localStorage.removeItem("ejercicio");
    localStorage.removeItem("dia");
    localStorage.removeItem("hora");
    localStorage.removeItem("play");
    let titulo;
    let idRutina = localStorage.getItem("idRutinaActual");
    this.firestoreService.getDatosRutina(idRutina).subscribe((rut) => {
      titulo = rut.payload.data().Nombre;
      let datos = {

        dia: this.Dia.titulo,
        idRutina: idRutina,
        nombreRutina: titulo,
        fecha: new Date(),
        duracion: duracion,
      }
      this.firestoreService.addHistorial(datos);
    });

    this.router.navigate(["menu/historial"]);

  }

  ejecutar(id, i) {
    if (this.play) {
      var borde = (document.getElementById('border' + i) as HTMLInputElement);
      borde.style.setProperty("--c", "red");
      this.hechos[i] = true;
      localStorage.setItem("hechos", JSON.stringify(this.hechos));
      let vector = new Array<Ejercicio>();
      for (let j = 0; j < this.vectorEjercicios[i].length; j++) {
        let a: Ejercicio;
        a = new Ejercicio(this.Superserie[this.vectorEjercicios[i][j]].nombre, this.Superserie[this.vectorEjercicios[i][j]].musculo, this.Superserie[this.vectorEjercicios[i][j]].descripcion, this.Superserie[this.vectorEjercicios[i][j]].serie, this.Superserie[this.vectorEjercicios[i][j]].repe, this.Superserie[this.vectorEjercicios[i][j]].repeticiones, this.Superserie[this.vectorEjercicios[i][j]].idEjercicio, this.Superserie[this.vectorEjercicios[i][j]].color, this.Superserie[this.vectorEjercicios[i][j]].dif, this.Superserie[this.vectorEjercicios[i][j]].num);
        vector.push(a);
        localStorage.setItem("ejercicio", JSON.stringify(vector));
        localStorage.setItem("dia", this.Dia.id);
        localStorage.setItem("hora", JSON.stringify(this.hora));
        localStorage.setItem("play", JSON.stringify(this.play));
        this.router.navigate(["ejecutar"]);
      }
    }
  }
}
