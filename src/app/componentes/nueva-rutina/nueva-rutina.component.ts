import { Component, OnInit, AfterViewInit, ViewChild, HostListener } from '@angular/core';
import { FirestoreService } from 'src/app/Servicio/firestore.service';
import { MatDialog } from '@angular/material';
import { NuevaComponent } from './modals/nueva/nueva.component';
import { CambiarNombreComponent } from './modals/cambiar-nombre/cambiar-nombre.component';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ComunicacionService } from 'src/app/servicio/comunicacion.service';
import { InfoComponent } from './modals/info/info.component';
import { EliminarComponent } from './modals/eliminar/eliminar.component';
import { ThemeService } from 'ng2-charts';
import { ignoreElements } from 'rxjs/operators';

export interface Nueva {
  nombre: string;
  fecha: string;
  duracion: string;
  dias: string;
}
export interface Ejer{
  imagen:string;
  nombre:string;
}
@Component({
  selector: 'app-nueva-rutina',
  templateUrl: './nueva-rutina.component.html',
  styleUrls: ['./nueva-rutina.component.css']
})
export class NuevaRutinaComponent implements OnInit, AfterViewInit {

  nombre;
  fecha;
  duracion;
  diasSemana;
  selectedTab;
  private param;
  private idRutina;
  private Dia = [];
  private Ejercicios = [];
  private eliminar:boolean;
  private fav:boolean;
  private edit:boolean;
  private nuevaPrincipal: boolean;
  private titulo;
  p = [];


  @HostListener('swipeleft', ['$event']) public swipePrev(event: any) {
    this.changeTabLeft();
  }
 
  @HostListener('swiperight', ['$event']) public swipeNext(event: any) {
    this.changeTabRight();
  }
  constructor(private firestoreService: FirestoreService, public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private comunicacionService: ComunicacionService) {
    this.selectedTab = 0;
    this.eliminar=false;
    this.fav=false;
    this.nuevaPrincipal=false;
    this.titulo="Nueva Rutina";
  }
  ngAfterViewInit() {
  }
  
  ngOnInit() {
    this.RecogerParametros();
    if (this.param == "0") {
      this.configuracionNuevaRutina();
      //this.subcripcion();
    } else {
      this.nuevaPrincipal=true;
      this.subDiasRutina();
    }
  }
  public marcarFav(){
    this.fav=true;
    this.firestoreService.cambiarRutinaActual(this.idRutina);
    this.router.navigate(["menu"]);
  }
  public editar(){
    this.edit=false;
  }
  public atras(){
    this.router.navigate(["menu/misRutinas"]);
  }
  public comprobarFav(){
     let idActal=localStorage.getItem("idRutinaActual");
     let idAhora=localStorage.getItem("idRutina");
     if(idActal==idAhora){
       this.fav=true;
     }
  }
  public pruebabotonSuperserie(index, i) {
    let colorAnterior=this.Ejercicios[index][i].color;
    colorAnterior=(colorAnterior+1)%4;
    this.Ejercicios[index][i].color=colorAnterior;
  }




  public RecogerParametros() {
    this.route.params.subscribe((params: Params) => {
      this.param = params.id;
      if(this.param==0 || this.param==1){
        this.edit=false;
      }else{
        this.edit=true;
      }
    })

  }
  changeTabRight() {
    this.selectedTab -= 1;
    if (this.selectedTab < 0) this.selectedTab = this.p.length;
  }
  changeTabLeft() {
    this.selectedTab += 1;
    if (this.selectedTab >= this.p.length) this.selectedTab = 0;
  }
  addTab() { 
    if (this.p.length == 7) {
      //disable boton +
    } else {
     this.firestoreService.crearDia(this.p.length,this.idRutina);
     this.firestoreService.updateRutinaAddNumDias(this.idRutina, this.p.length+1);
    }
  }
  removeTab() {
    this.firestoreService.deleteDia(this.idRutina,this.Dia[this.Dia.length-1].id);
    this.firestoreService.updateRutinaQuitarNumDias(this.idRutina, this.Dia.length-1);
  }
  addEjercicio(index) {
    localStorage.setItem("selectedTab",this.selectedTab);
    this.comunicacionService.setIdRutina(this.idRutina);
    this.comunicacionService.setIdDiaRutina(this.Dia[index].id);
    this.router.navigate(["menu/ejercicios", "1"]);
  }
  
  guardar(){
    localStorage.removeItem("selectedTab");
    for(let i=0; i<this.Dia.length;i++){
      let j=0;
      if(this.Ejercicios[i].length!=0){
        let data={
          Titulo:this.Dia[i].titulo,
          Numero: this.Dia[i].numero,
          Ejercicios:[],
          Vector:[],
        }
        let data1={
          y:{
            Dif:"",
            IdEjercicio:"",
            Referencina:"",
            Repe:false,
            Repeticiones:[],
            Serie:2,
            Num: 1,
          }
        }
        let vectorEjer=[];
        let vectorColor=[];
        while(j<this.Ejercicios[i].length){//si no se mete es que no tiene ejercicios no tengo que modificar nada
            let data={
              y:{
                Dif:this.Ejercicios[i][j].dif,
                IdEjercicio:this.Ejercicios[i][j].idEjercicio,
                Repe:this.Ejercicios[i][j].repe,
                Repeticiones:this.Ejercicios[i][j].repeticiones,
                Referencia:this.Ejercicios[i][j].referencia,
                Serie:this.Ejercicios[i][j].serie,
                Num: this.Ejercicios[i][j].num,
               },
            };
            let idAleaotio=this.firestoreService.generarID();
            let color=this.Ejercicios[i][j].color+"-"+idAleaotio;
            vectorColor.push(color);
            vectorEjer.push(data);
            j++;
        }
        data.Ejercicios=vectorEjer;
        console.log("antes de guardar")
        console.log(vectorEjer);
        data.Vector=vectorColor;
        this.firestoreService.updateDia(this.idRutina,this.Dia[i].id, data);
      }
    }
    this.router.navigate(["menu/misRutinas"]);
  }
  public cambiarNombre(index) {
    localStorage.setItem("selectedTab",this.selectedTab);
    const dialogRef = this.dialog.open(CambiarNombreComponent, {
      width: '374px',
      height: 'auto'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.idRutina = this.comunicacionService.getidRutina();
        this.p[index].titulo = result.nombre;
        let data = {
          Titulo: result.nombre,
        }
        this.firestoreService.updateDia(this.idRutina, this.Dia[index].id, data);

      }
      this.selectedTab=parseInt(localStorage.getItem("selectedTab"));
    })
    //this.subcripcion();
  }
  public ocultarBoton() {
    var elemento = (document.getElementById("menu") as HTMLButtonElement);
    elemento.style.display = "none";
  }

  public configuracionNuevaRutina() {
    const dialogRef = this.dialog.open(NuevaComponent, {
      width: '374px',
      height: 'auto',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.nombre = result.nombre;
      this.fecha = result.fecha;
      this.duracion = result.duracionSemanas;
      this.diasSemana = result.diasEntrenamiento;
      let _idRutina = this.firestoreService.crearRutina(result);
      this.idRutina = _idRutina;
      for (let i = 0; i < this.diasSemana; i++) {
        let numero = i + 1;
        let idDiaRutina = this.firestoreService.crearDia(numero, _idRutina);

      }
      this.comunicacionService.setIdRutina(_idRutina);
      this.titulo=result.nombre;
      this.nuevaPrincipal=true;
      this.subcripcion();
      //funcion que recoge  rutinas del dia nose si lllamarla aqui o con el aftefView lo cogeria tengo que hacer prueba gordas
    });
  }
  botonEliminar(){
    if(this.eliminar){
      this.eliminar=false;
      for(let  i=0;i<this.Ejercicios.length;i++){
        for(let j=0;j<this.Ejercicios[i].length;j++){
          this.Ejercicios[i][j].eliminar=false;
        }
      }
    }else{
      this.eliminar=true;
    }
  }
  public showEliminar(index,id){
    const dialogRef = this.dialog.open(EliminarComponent,{
      width: '374px',
      height: 'auto',
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result!=undefined){
        //eliminar de la base de datos
        for(let i=0; i<this.Ejercicios.length;i++){
          let j=0;
          while(j<this.Ejercicios[i].length){
            if(this.Ejercicios[i][j].eliminar==true){
              let data={
                y:{
                  IdEjercicio:this.Ejercicios[i][j].idEjercicio,
                  Repe:this.Ejercicios[i][j].repe,
                  Repeticiones:this.Ejercicios[i][j].repeticiones,
                  Referencia:this.Ejercicios[i][j].referencia,
                  Serie:this.Ejercicios[i][j].serie,
                  Dif:this.Ejercicios[i][j].dif,
                  Num:this.Ejercicios[i][j].num,
                 },
              };
              let data1=this.Dia[i].vector[j];
              this.firestoreService.deleteEjerRutina(this.Dia[i].id,this.idRutina,data,data1);
              j++;
            }else{
              j++;
            }
          }
        }
        this.eliminar=false;
        this.subDiasRutina();
      }
    });
  }
  public showInfo(){
    const dialogRef = this.dialog.open(InfoComponent, {
      width: '374px',
      height: 'auto',
    });
  }
  public subcripcion() {
    this.p = [];
    this.Dia = [];
    this.Ejercicios=[];
    this.idRutina = this.comunicacionService.getidRutina();
    this.firestoreService.getDatosRutina(this.idRutina).subscribe((r)=>{
     this.titulo = r.payload.data().Nombre;
    })
    this.firestoreService.getRutina(this.idRutina).subscribe((rutDias) => {
      this.p = [];
      this.Dia = [];
      this.Ejercicios=[];
      let vector=[];
      let i = 0;
      rutDias.forEach((dia: any) => {
        this.Dia.push({
          id: dia.payload.doc.id,
          titulo: dia.payload.doc.data().Titulo,
          numero:dia.payload.doc.data().Numero,
          data: dia.payload.doc.data(),
          vector:dia.payload.doc.data().Vector,
        });
       
        let titulo1 = "Día " + (i + 1);
        let index = i + 1;
        let data = {
          titulo: titulo1,
          tab: index
        }
        this.p.push(data);
        
        try{
          vector=[];
          let j=0;
          for(let ejer of this.Dia[i].data.Ejercicios){
            let ejercicio = {  
              nombre:"espera",
              musculo:"espera",
              serie: ejer.y.Serie,
              repe:ejer.y.Repe,
              repeticiones:ejer.y.Repeticiones,
              referencia:ejer.y.Referencia,
              idEjercicio:ejer.y.IdEjercicio,
              dif:ejer.y.Dif,
              num:ejer.y.Num,
              color: 0,
              eliminar: false,
              imagen:"",
            }
            
           
            let codColor: string;
            codColor=this.Dia[i].vector[j];
            codColor.charAt(0);
            ejercicio.color=parseInt(codColor);
            ejer.y.Referencia.get().then(documentSnapshot => {
              if(documentSnapshot.exists){
                ejercicio.nombre=documentSnapshot.data().Nombre;
                ejercicio.musculo=documentSnapshot.data().Tipo;
                ejercicio.imagen=this.imagenes(ejercicio.musculo);
              }
            });
            vector.push(ejercicio);
            j++;
          }
          i++;
          this.Ejercicios.push(vector);
        }catch(e){
          let vectorVacido=[];
          this.Ejercicios.push(vectorVacido);
          i++;
        }
      })
    });
  }
  public subDiasRutina() {
    if (this.param == "0") {
      this.subcripcion()
    } else if (this.param == "1") {
      this.subcripcion();
      this.selectedTab=parseInt(localStorage.getItem("selectedTab"));
    }else{
      this.idRutina=this.param;
      this.comunicacionService.setIdRutina(this.param);
      let idRuinaActual=localStorage.getItem("idRutinaActual");
      if(this.idRutina==idRuinaActual){
        this.fav=true;
      }
      this.subcripcion()
    }
  }
  imagenes(tipo){
    switch(tipo){
      case "Abdomen":
        return "assets/imagenes/tipo/abdomen.png";
      case "Biceps":
        return "assets/imagenes/tipo/biceps.png";
      case 'Cinta':
          return "assets/imagenes/tipo/cinta.png";
      case 'Bicicleta':
          return "assets/imagenes/tipo/bici.png";
      case 'Eliptica':
          return "assets/imagenes/tipo/eliptica.png";
      case 'Espalda':
        return "assets/imagenes/tipo/espalda.png";
      case 'Hombro':
        return "assets/imagenes/tipo/hombro2.png";
      case 'Pecho':
        return "assets/imagenes/tipo/pecho2.png";
      case 'Piernas Superiores':
        return "assets/imagenes/tipo/pierna-c.png";
        case 'Piernas Inferiores':
        return "assets/imagenes/tipo/pierna-gemelo.png";
      case 'Oblicuos':
        return "assets/imagenes/tipo/oblicuos.png";
      case 'Triceps':
        return "assets/imagenes/tipo/triceps.png";
    }

  }
}
