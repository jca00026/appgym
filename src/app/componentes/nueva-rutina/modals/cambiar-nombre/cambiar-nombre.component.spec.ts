import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CambiarNombreComponent } from './cambiar-nombre.component';

describe('CambiarNombreComponent', () => {
  let component: CambiarNombreComponent;
  let fixture: ComponentFixture<CambiarNombreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambiarNombreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CambiarNombreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
