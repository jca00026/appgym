import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from 'src/app/componentes/peso/peso.component';


@Component({
  selector: 'app-cambiar-nombre',
  templateUrl: './cambiar-nombre.component.html',
  styleUrls: ['./cambiar-nombre.component.css']
})
export class CambiarNombreComponent implements OnInit {
  private diaForm: FormGroup;
  constructor(public dialogRef: MatDialogRef<any>,private formBuilder: FormBuilder) {
      dialogRef.disableClose = true;
      this.diaForm=this.formBuilder.group({
      nombre:['',Validators.required],
    });
     }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  addNombre(form){
    if(this.comprobarCampos()){
        this.dialogRef.close(this.diaForm.value);
    }
  }
  comprobarCampos():boolean{
    var result: boolean=true;
    var nombre = (document.getElementById('nombre') as HTMLInputElement);
    var fecha= (document.getElementById('fecha')as HTMLInputElement);
    var duracion=(document.getElementById('duracion')as HTMLInputElement);
    var dias=(document.getElementById('dias')as HTMLInputElement);
    if(this.diaForm.get("nombre").invalid){
      nombre.style.setProperty("--c","red");
      result=false;
    }
    return result;
  }
}
