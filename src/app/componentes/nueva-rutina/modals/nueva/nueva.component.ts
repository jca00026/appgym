import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from '../../../peso/peso.component';
import { Nueva } from '../../nueva-rutina.component';

@Component({
  selector: 'app-nueva',
  templateUrl: './nueva.component.html',
  styleUrls: ['./nueva.component.css']
})
export class NuevaComponent implements OnInit {
  selected;
  selected1;
  private rutinaForm: FormGroup;
  constructor(public dialogRef: MatDialogRef<Nueva>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private formBuilder: FormBuilder) { 
      dialogRef.disableClose = true;
      let fecha= new Date();
      this.rutinaForm=this.formBuilder.group({
      nombre:['',Validators.required],
      fecha:[fecha],
      duracionSemanas:['',Validators.required],
      diasEntrenamiento:['',Validators.required]
    });


  }
  
  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  addRutina(form){
    if(this.comprobarCampos()){
        this.dialogRef.close(this.rutinaForm.value);
    }
  }
  comprobarCampos():boolean{
    var result: boolean=true;
    var nombre = (document.getElementById('nombre') as HTMLInputElement);
    //var fecha= (document.getElementById('fecha')as HTMLInputElement);
    var duracion=(document.getElementById('duracion')as HTMLInputElement);
    var dias=(document.getElementById('dias')as HTMLInputElement);
    if(this.rutinaForm.get("nombre").invalid && this.rutinaForm.get("fecha").invalid
    && this.rutinaForm.get("duracionSemanas").invalid && this.rutinaForm.get("diasEntrenamiento").invalid){
     
      nombre.style.setProperty("--c","red");
      //fecha.style.setProperty("--c","red");
      duracion.style.setProperty("--p","red");
      dias.style.setProperty("--p","red");
      result=false;
    }else{
      if(this.rutinaForm.get("nombre").invalid){
        nombre.style.setProperty("--c","red");
        result=false;
      }
     
      if(this.rutinaForm.get("duracionSemanas").invalid){
        duracion.style.setProperty("--p","red");
        result=false;
      }
      if(this.rutinaForm.get("diasEntrenamiento").invalid){
        dias.style.setProperty("--p","red");
        result=false;
      }
    } 
    return result;
  }

}
