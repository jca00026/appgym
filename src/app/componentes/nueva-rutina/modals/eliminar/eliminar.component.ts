import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Ejer } from '../../nueva-rutina.component';



@Component({
  selector: 'app-eliminar',
  templateUrl: './eliminar.component.html',
  styleUrls: ['./eliminar.component.css']
})
export class EliminarComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<Ejer>) {
    var cambio=(document.getElementsByClassName('mat-dialog-container'));
    var elemento;
    for(var i=0;i<1;i++){
      elemento=cambio[i];
    }
    (elemento as HTMLSelectElement).style.backgroundColor="black";
     }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  eliminar(){
    this.dialogRef.close(true);
  }
}
