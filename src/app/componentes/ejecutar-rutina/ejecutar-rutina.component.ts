import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FirestoreService } from 'src/app/Servicio/firestore.service';
import { MatDialog } from '@angular/material';
import { MuestraHistoriaComponent } from '../muestra-historia/muestra-historia.component';
import { Ejercicio } from 'src/app/clases/ejercicio';
import { NumberValueAccessor } from '@angular/forms';
import { ignoreElements } from 'rxjs/operators';

@Component({
  selector: 'app-ejecutar-rutina',
  templateUrl: './ejecutar-rutina.component.html',
  styleUrls: ['./ejecutar-rutina.component.css']
})
export class EjecutarRutinaComponent implements OnInit {

  private idDia;
  private ejercicios: Array<Ejercicio>;
  private serie;
  private repeticion = [];
  private maxSerie;
  private carga: Array<Array<number>>;

  constructor(private router: Router, public dialog: MatDialog, private firestoreService: FirestoreService) {
    this.serie = 1;
    this.ejercicios = JSON.parse(localStorage.getItem("ejercicio"));
    this.idDia = localStorage.getItem("dia");
    this.maxSerie = this.ejercicios[0].serie;
    this.carga = new Array<Array<number>>();
    for (let i = 0; i < this.ejercicios.length; i++) {
      this.carga.push(new Array<number>());
      for (let j = 0; j < this.maxSerie; j++) {
        this.carga[i].push(null);
      }
    }
  }

  ngOnInit() {

  }
  atrar() {
    //elimniar hechos o poner el i que este hecho en false;
    this.router.navigate(["dia/", this.idDia]);
  }
  siguinte() {
    let ok: boolean;
    ok = true;
    for (let i = 0; i < this.ejercicios.length; i++) {
      let input = (document.getElementById('carga' + i) as HTMLInputElement);
      if (this.ejercicios[i].musculo == "Bici" || this.ejercicios[i].musculo == "Cinta" || this.ejercicios[i].musculo == "Eliptica") {

      } else {
        if (input.value == "") {
          ok = false;
          input.style.setProperty("--c", "red");
        } else {
          this.carga[i][this.serie - 1] = parseInt(input.value);
        }
      }
    }
    if (ok) {
      this.serie++;
    }
  }
  add(i) {
    let carga = (document.getElementById('carga' + i) as HTMLInputElement);
    if (carga.value == "") {
      let number = 0;
      number += 1;
      carga.value = "" + number;
    } else {
      let number = parseInt(carga.value);
      number += 1;
      carga.value = "" + number;
    }

  }
  remove(i) {
    let carga = (document.getElementById('carga' + i) as HTMLInputElement);
    if (carga.value == "" || carga.value == "0") {

    } else {
      let number = parseInt(carga.value);
      number -= 1;
      carga.value = "" + number;
      if (number == 0) {
        carga.value = "";
      }
    }

  }
  terminar() {
    let ok: boolean;
    ok = true;
    for (let i = 0; i < this.ejercicios.length; i++) {
      if (this.ejercicios[i].musculo == "Bici" || this.ejercicios[i].musculo == "Cinta" || this.ejercicios[i].musculo == "Eliptica") {

      } else {
        let input = (document.getElementById('carga' + i) as HTMLInputElement);
        if (input.value == "") {
          ok = false;
          input.style.setProperty("--c", "red");
        } else {
          this.carga[i][this.serie - 1] = parseInt(input.value);
        }
      }
    }
    if (ok) {
      for (let i = 0; i < this.ejercicios.length; i++) {
        let data = {
          r: this.ejercicios[i].repeticiones,
          c: this.carga[i],
          fecha: new Date(),
        }
        if (this.ejercicios[i].musculo == "Bici" || this.ejercicios[i].musculo == "Cinta" || this.ejercicios[i].musculo == "Eliptica") {
        
        }else{
          this.firestoreService.addHitorialEjercicio(this.ejercicios[i].idEjercicio, data); 
        }
      }
      this.router.navigate(['dia/', this.idDia]);
    }


  }
  show(i) {
    if(this.ejercicios[i].musculo == "Bici" || this.ejercicios[i].musculo == "Cinta" || this.ejercicios[i].musculo == "Eliptica"){

    }else{
      const dialogRef = this.dialog.open(MuestraHistoriaComponent, {
        width: '400px',
        height: '450px',
        data: { id: this.ejercicios[i].idEjercicio }
      });
      dialogRef.afterClosed().subscribe(result => {
  
      })
  
    }
    
  }
}

