import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EjecutarRutinaComponent } from './ejecutar-rutina.component';

describe('EjecutarRutinaComponent', () => {
  let component: EjecutarRutinaComponent;
  let fixture: ComponentFixture<EjecutarRutinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EjecutarRutinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EjecutarRutinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
