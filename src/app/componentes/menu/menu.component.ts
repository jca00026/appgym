import { Component, OnInit, AfterViewInit } from '@angular/core';
import {ComunicacionService} from '../../servicio/comunicacion.service'
import { FirestoreService } from 'src/app/Servicio/firestore.service';
import { MatSidenavContainer, MatSidenav } from '@angular/material';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit,AfterViewInit {
  
  public anch;
  public titulo;
  private alt;
  private width;
  constructor(private comunicacionService: ComunicacionService, private firestoreService: FirestoreService) { 
   
  }
  
  public mostrarBoton(){
    var elemento=(document.getElementById("menu") as HTMLButtonElement );
    elemento.style.display="inline-block;";
  }
  ngAfterViewInit(): void {

    this.mostrarBoton();
  }
  ngOnInit() {
    this.alt=screen.height;
    this.width=screen.width;
    this.comunicacionService.custommessage.subscribe(titulo => this.titulo=titulo);
    this.titulo=localStorage.getItem("titulo");
    
  }
  cambiarTitulo(nombre){
    localStorage.setItem("titulo",nombre);
    this.comunicacionService.setTitulo(nombre);
    
  }
  logout(){
    this.firestoreService.logout();
  }
}
