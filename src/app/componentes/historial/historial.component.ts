import { Component, OnInit, ViewEncapsulation, ViewChild, EventEmitter, AfterViewInit, Renderer2, ChangeDetectionStrategy, Inject, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatCalendarCellCssClasses, MatCalendar, MAT_DATE_FORMATS, DateAdapter, MatDateFormats } from '@angular/material';
import {takeUntil} from 'rxjs/operators';
import { FirestoreService } from 'src/app/Servicio/firestore.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { ComunicacionService } from 'src/app/servicio/comunicacion.service';
import {animate,keyframes,query,stagger,state,style,transition,trigger} from '@angular/animations';



export const PICK_FORMATS = {
  parse: {dateInput: {month: 'long ', year: 'numeric', day: 'numeric'}},
  display: {
      dateInput: 'input',
      monthYearLabel: {year: 'numeric', month: 'long'},
      dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
      monthYearA11yLabel: {year: 'numeric', month: 'long'}
  }
};

export const speedDialFabAnimations = [
  trigger('fabToggler', [
    state('inactive', style({
      transform: 'rotate(0deg)'
    })),
    state('active', style({
      transform: 'rotate(225deg)'
    })),
    transition('* <=> *', animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
  ]),
  trigger('speedDialStagger', [
    transition('* => *', [

      query(':enter', style({ opacity: 0 }), {optional: true}),

      query(':enter', stagger('40ms',
        [
          animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)',
            keyframes(
              [
                style({opacity: 0, transform: 'translateY(10px)'}),
                style({opacity: 1, transform: 'translateY(0)'}),
              ]
            )
          )
        ]
      ), {optional: true}),

      query(':leave',
        animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)',
          keyframes([
            style({opacity: 1}),
            style({opacity: 0}),
          ])
        ), {optional: true}
      )

    ])
  ])
];
@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css'],
  providers: [{provide: MAT_DATE_FORMATS, useValue: PICK_FORMATS}],
  animations: speedDialFabAnimations
})


export class HistorialComponent implements OnInit, AfterViewInit {

  exampleHeader=HeaderComponent;
  
  @ViewChild('calendar', { static: false }) calendar: MatCalendar<Date>;
  private estatodo: boolean;
  private mes;
  private historial: Map<string,any>;
  private month: Array<any>;
  datesToHighlight = [] ;
  constructor(private renderer: Renderer2, private firestoreService: FirestoreService, private route: Router, private comunicacionService: ComunicacionService) {
   this.historial = new Map();
   this.month= new Array();
   this.estatodo=false;
   this.subHistorial();
  }
  
  ngOnInit() {
    this.cambiarTitulo("Historial");
    this.mostrarBoton();
  }
  public mostrarBoton(){
    var elemento=(document.getElementById("menu") as HTMLButtonElement );
    elemento.style.display="inline-block";
  }
   preparacionCalendarioDiasHistorial(mesanio){   
    this.month=[];
    for(let i of this.historial.values()){
      let opt = {
      year: "numeric", month: "long"
      };
      let y = new Date(i.fecha.seconds * 1000);
      let fecha=y.toLocaleString('es-MX',opt);
      if(mesanio==fecha){
        let options = {
          weekday: 'short',
        };
        let nombreDia= y.toLocaleString('es-MX',options);
        nombreDia=nombreDia.substr(0, nombreDia.length-1);
        nombreDia=nombreDia.charAt(0).toUpperCase() + nombreDia.slice(1);
        let options1 = {
          day: 'numeric' 
        };
        let numerodia=y.toLocaleString('es-MX',options1);
        let data={
          dia:nombreDia,
          numeroDia:parseInt(numerodia),
          Duracion: i.Duracion,
          nombreRutina: i.nombreRutina,
          diaRutina: i.dia
        }
        this.month.push(data);
      } 
     }
     let funComparation=(n1,n2)=>{
      if(n1.numeroDia<n2.numeroDia) return 1;
      if(n1.numeroDia>n2.numeroDia) return -1;
      return 0
    }
    console.log(this.month);
    let prueba= this.month.sort(funComparation);
    console.log(this.month);
   }

 
  //funcion apra pasarle al calendario los dias seleccionados
  dateClass() {
    
    return (date: Date): MatCalendarCellCssClasses => {
      const highlightDate = this.datesToHighlight
        .map(strDate => new Date(strDate))
        .some(d => d.getDate() === date.getDate() && d.getMonth() === date.getMonth() && d.getFullYear() === date.getFullYear());
      return highlightDate ? 'highlight-dates a' : '';
    };
  }
  ngAfterViewInit() {
    this.comunicacionService.dateMes.subscribe(message =>{
        this.preparacionCalendarioDiasHistorial(message);
    })
  }
  public subHistorial(){
    this.firestoreService.getHirtorial().subscribe((vecHistorial)=>{
      vecHistorial.forEach((record:any)=>{
        let id= record.payload.doc.id;
        let data = record.payload.doc.data();
        this.historial.set(id as string ,data);
      })
     for(let i of this.historial.values()){
      let y = new Date(i.fecha.seconds * 1000);
      this.datesToHighlight.push(y.toISOString());
     }
     let today=new Date()
     let opt = {
      year: "numeric", month: "long"
      };
     let mesanio=today.toLocaleString('es-MX',opt);
     this.preparacionCalendarioDiasHistorial(mesanio);
     this.estatodo=true;
    });
  }
  cambiarTitulo(nombre){
    localStorage.setItem("titulo",nombre);
    this.comunicacionService.setTitulo(nombre);
  }
  fabButtons = [
    {
      label: "Peso"
    },
  ];
  buttons = [];
  fabTogglerState = 'inactive';
  showItems() {
    this.fabTogglerState = 'active';
    this.buttons = this.fabButtons;
  }
  hideItems() {
    this.fabTogglerState = 'inactive';
    this.buttons = [];
  }
  onToggleFab() {
    this.buttons.length ? this.hideItems() : this.showItems();
  }
  fabButton(nameButton){
    if(nameButton=="Peso"){
      this.route.navigate(['menu/peso',"add"]);
    }else{
      
    }
    this.onToggleFab();
  }
  
}

@Component({
  selector: 'calendar-header' ,
  styles: [`
    .example-header {
      display: flex;
      align-items: center;
      padding: 0.5em;
    }

    .example-header-label {
      flex: 1;
      height: 1em;
      font-weight: 500;
      text-align: center;
    }

    .example-double-arrow .mat-icon {
      margin: -22%;
    }
  `],
  templateUrl:'header.component.html' ,
  exportAs:'HeaderComponent' ,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent<D> implements OnDestroy  {
  private _destroyed = new Subject<void>();
  constructor(private comunicacionService: ComunicacionService,
    private _calendar: MatCalendar<D>,
              private _dateAdapter: DateAdapter<D>,
              @Inject(MAT_DATE_FORMATS) private _dateFormats: MatDateFormats,changeDetectorRef: ChangeDetectorRef) {

                this._calendar.stateChanges.subscribe(() => changeDetectorRef.markForCheck());
              }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }
  get periodLabel() {
    return this._dateAdapter
        .format(this._calendar.activeDate,this._dateFormats.display.monthYearLabel)
        .toLocaleUpperCase();
  }

  previousClicked(mode: any) {
    this._calendar.activeDate = this._dateAdapter.addCalendarMonths(this._calendar.activeDate, -1);
    let p=this._dateAdapter.format(this._calendar.activeDate,this._dateFormats.display.monthYearA11yLabel);
    this.comunicacionService.cambiaMes(p);
  }

  nextClicked(mode: any) {
    this._calendar.activeDate =  this._dateAdapter.addCalendarMonths(this._calendar.activeDate, 1);
    let p=this._dateAdapter.format(this._calendar.activeDate,this._dateFormats.display.monthYearA11yLabel);
    this.comunicacionService.cambiaMes(p);
  }
}
