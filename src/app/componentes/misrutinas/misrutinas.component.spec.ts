import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisrutinasComponent } from './misrutinas.component';

describe('MisrutinasComponent', () => {
  let component: MisrutinasComponent;
  let fixture: ComponentFixture<MisrutinasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisrutinasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisrutinasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
