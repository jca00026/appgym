import { Component, OnInit } from '@angular/core';
import { Router, ChildActivationStart } from '@angular/router';
import { FirestoreService } from 'src/app/Servicio/firestore.service';
import { ComunicacionService } from 'src/app/servicio/comunicacion.service';
import { EliminarComponent } from '../nueva-rutina/modals/eliminar/eliminar.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-misrutinas',
  templateUrl: './misrutinas.component.html',
  styleUrls: ['./misrutinas.component.css']
})
export class MisrutinasComponent implements OnInit {
  botonstate='inactive';
  private idRutinaFav:string;
  private idActive;
  private Rutinas=[];
  private eli: boolean;
  
  constructor(private router:Router, private comunicacionService: ComunicacionService, private firesstoreService: FirestoreService, public dialog: MatDialog) { 
    this.eli=false;
  }

  ngOnInit() {
    this.cambiarTitulo("Mis Rutinas");
   this.rutinaPrincipal();
    this.subRutina();
  }
  cambiarTitulo(nombre){
    localStorage.setItem("titulo",nombre);
    this.comunicacionService.setTitulo(nombre);
  }
  addNuevaRutina(){
    localStorage.setItem("titulo", "Nueva Rutina");
    this.comunicacionService.setTitulo("Nueva Rutina");

    this.router.navigate(['nuevaRutina',"0"]);
  }
  eliminarElementos(){
    if(this.eli){
      this.eli=false;
      for(let i=0; i<this.Rutinas.length;i++){
        this.Rutinas[i].eliminar=false;
      }
    }else{
      this.eli=true;
    }
  }
  onToggleFab() {
    this.botonstate='inactive';
    let id=this.idActive;
    let star=(document.getElementById(id+"s") as HTMLButtonElement );
    let see=(document.getElementById(id+"v") as HTMLButtonElement);
    let edit=(document.getElementById(id+"e") as HTMLButtonElement );
    star.style.display="none";
    see.style.display="none";
    edit.style.display="none";
    this.idActive="";
  }
  pulsarRutina(index){
    let id=this.Rutinas[index].id;
    this.router.navigate(["nuevaRutina",id]);
  }
  rutinaFav(id){
    let favantiguo=(document.getElementById(this.idRutinaFav) as HTMLButtonElement );
    favantiguo.style.display="none";
    this.idRutinaFav=id;
    let favnuevo=(document.getElementById(this.idRutinaFav) as HTMLButtonElement );
    favnuevo.style.display="inline-block";
    this.onToggleFab();
  }
  add(){

  }
   subRutina(){
    this.Rutinas=[];
      this.firesstoreService.getRutinas().subscribe((ruts) => {
        this.Rutinas=[];
       ruts.forEach((rut) => {
         let data = {
           id: rut.payload.doc.id,
           nombre: rut.payload.doc.data().Nombre,
           dias: rut.payload.doc.data().DiasEntrenar,
           fav: false,
           eliminar:false,
         };
         if (this.idRutinaFav == data.id) {
           data.fav = true;
         }
         this.Rutinas.push(data);
       });
       for (let i=0; i < this.Rutinas.length; i++) {
         
         if (this.Rutinas[i].id == this.idRutinaFav) {
          
           if (i != 0) {
            
             let rutina = this.Rutinas[0];
             this.Rutinas[0] = this.Rutinas[i];
             this.Rutinas[i] = rutina;
           }
         }
       }
     });
  }
  rutinaPrincipal(){
    this.idRutinaFav=localStorage.getItem("idRutinaActual");
  }

  mensajeError(){
    const dialogRef = this.dialog.open(EliminarComponent,{
      width: '374px',
      height: 'auto',
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result!=undefined){
       for(let i=0;i<this.Rutinas.length;i++){
         if(this.Rutinas[i].eliminar==true){
           this.firesstoreService.deleteRutina(this.Rutinas[i].id);
           this.Rutinas.splice(i,1);
         }
       }
       console.log(this.Rutinas.length);
       if(this.Rutinas.length==0){
        this.firesstoreService.cambiarRutinaActual("");
       }
       this.eli=false;
       this.Rutinas=[];
      }
    });
  }
}
