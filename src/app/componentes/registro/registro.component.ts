import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { FirestoreService } from 'src/app/Servicio/firestore.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  registroForm: FormGroup;
  
  constructor(private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private firestore: FirestoreService) { }

  ngOnInit() {
    this.registroForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required,Validators.minLength(8)]],
      rpassword: ['',Validators.required]
    });
  }
  Registrarse(){
    if(this.comprobarCampos()){
      let email= this.registroForm.get('email').value;
      let pass= this.registroForm.get('password').value;
      this.firestore.register(email,pass);
      
    }
  }
  comprobarCampos(): boolean{
    var result: boolean =false;
    var email=(document.getElementById('email') as HTMLInputElement);
    var p1=(document.getElementById('p1') as HTMLInputElement);
    var p2=(document.getElementById('p2') as HTMLInputElement);
    if(this.registroForm.get('email').invalid){
      if(this.registroForm.get('email').errors.required){
        email.value="";
        email.placeholder="Email";
        email.style.setProperty("--c", "red");
      }
      if(this.registroForm.get('email').errors.email){
        email.value="";
        email.placeholder="Email no valido";
        email.style.setProperty("--c", "red");
      }
    }
    if(this.registroForm.get('password').invalid){
      let pass:string;
      pass=this.registroForm.get('password').value;
      if(pass.length<8 && pass.length>0){
        p1.value="";
        p1.placeholder="Minimo 8 caracteres";
        p1.style.setProperty("--c", "red");
      }else{
        p1.placeholder="Contraseña";
        p1.style.setProperty("--c", "red");
      }
    }else{
      let pass:string;
      let pass2:string;
      pass2=this.registroForm.get("rpassword").value;
      pass=this.registroForm.get('password').value;
      if(pass!=pass2){
        p1.value="";
        p1.placeholder="Contraseña no coincide";
        p1.style.setProperty("--c", "red");
        p2.value="";
        p2.placeholder="Contraseña no coincide";
        p2.style.setProperty("--c", "red");
      }else{
        return true;
      }
    }
    return result;
  }
}
