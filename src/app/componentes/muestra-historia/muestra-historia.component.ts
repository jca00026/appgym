import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FirestoreService } from 'src/app/Servicio/firestore.service';

export interface Ejer{
Nombre:string,
Tipo:string,
Description:string,
Hist:Array<any>,
}
@Component({
  selector: 'app-muestra-historia',
  templateUrl: './muestra-historia.component.html',
  styleUrls: ['./muestra-historia.component.css']
})
export class MuestraHistoriaComponent implements OnInit {

  private idEjercicio;
  private ejercicio: Ejer;
  private nombre:string;
  private Hist=[];
  private fecha=[];

  constructor(public dialogRef: MatDialogRef<string>, @Inject(MAT_DIALOG_DATA) public data: string, private firestoreService: FirestoreService) { 
    this.idEjercicio=data
      
  }

  ngOnInit() {
    this.firestoreService.getEjercicio(this.idEjercicio.id).subscribe((ejer)=>{
      this.Hist=ejer.payload.data().Hist;
      
      let data={
        nombre:ejer.payload.data().Nombre,
        musculo:ejer.payload.data().Tipo,
        descripcion:ejer.payload.data().Description,
        historial:ejer.payload.data().Hist,
      }
      this.ejercicio=ejer.payload.data();
      this.Hist.sort((n1,n2)=>{
        if(new Date(n1.fecha.seconds*1000)<new Date(n2.fecha.seconds*1000)) return 1;
        if(new Date(n1.fecha.seconds*1000)>new Date(n2.fecha.seconds*1000)) return -1;
        return 0;
      });
      for(let fi of this.Hist){
        
        let _fecha= new Date(fi.fecha.seconds* 1000);
        let options = {
          day:"2-digit",
          month: "2-digit",
          year:"numeric",
        };
        let f= _fecha.toLocaleString('es-MX',options);
        this.fecha.push(f);
      }

    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  
}
