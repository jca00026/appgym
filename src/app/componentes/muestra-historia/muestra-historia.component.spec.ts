import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuestraHistoriaComponent } from './muestra-historia.component';

describe('MuestraHistoriaComponent', () => {
  let component: MuestraHistoriaComponent;
  let fixture: ComponentFixture<MuestraHistoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuestraHistoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuestraHistoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
