import { Component, OnInit } from '@angular/core';
import { FirestoreService } from 'src/app/Servicio/firestore.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ComunicacionService } from 'src/app/servicio/comunicacion.service';
import { firestore } from 'firebase';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {
  private Dia = [];
  private user;
  private nuevo:boolean;
  private numRutinas :number;
  constructor(
    private firestoreService: FirestoreService,
    private route: ActivatedRoute,
    private router: Router,
    private comunicacionService: ComunicacionService) {
      localStorage.removeItem("idRutina");
      localStorage.removeItem("idDiaRutina");
      this.numRutinas=0;
     }


  ngOnInit() {
    this.cambiarTitulo("Rutina Actual");
    this.mostrarBoton();
    this.firestoreService.getDataUser().subscribe((usuario)=>{
      this.user={
        datos: usuario.payload.data(),
      };
      localStorage.setItem("idRutinaActual",this.user.datos.RutinaActual);
      if(this.user.datos.RutinaActual==""){
        this.SubRutinas();
        
          this.nuevo=true;
        
      }else{
        this.SubRutinaDia(this.user.datos.RutinaActual);
      }
    });
   
  }
  public SubRutinas(){
    this.firestoreService.getRutinas().subscribe( (rut)=> {
      rut.forEach(r =>{
        this.numRutinas++;
      })
    })
  }
  public addRutina(){
    //redirigir a nueva rutina
    this.router.navigate(["nuevaRutina", "0"]);
  }
  public mostrarBoton(){
    var elemento=(document.getElementById("menu") as HTMLButtonElement );
    elemento.style.display="inline-block";
  }
  cambiarTitulo(nombre){
    localStorage.setItem("titulo",nombre);
    this.comunicacionService.setTitulo(nombre);
  }
  private SubRutinaDia(idRutina) {
    this.firestoreService.getRutina(idRutina).subscribe((rutDias) => {
      rutDias.forEach((dia: any) => {
        this.Dia.push({
          id: dia.payload.doc.id,
          titulo: dia.payload.doc.data().Titulo
        });
      })
    });
  }



  public redirigir(idDia, titulo) {
    localStorage.setItem("titulo", titulo);
    this.comunicacionService.setTitulo(titulo);

    this.router.navigate(['dia/', idDia]);
  }
 
}
