import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from "@angular/router";
import { auth, firestore } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from 'firebase';
import { map } from 'rxjs/operators';
import { Ejer } from '../componentes/muestra-historia/muestra-historia.component';
import { Rutina } from '../componentes/dia/dia.component';
import { DIA } from '../componentes/ejercicios/ejercicios.component';


@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  private user: User;
  private usuario;
  constructor(private firestore: AngularFirestore, private afAuth: AngularFireAuth, private router: Router) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.user = user;
        localStorage.setItem('user', JSON.stringify(this.user));
        this.usuario = JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
      }
    })
    this.usuario = JSON.parse(localStorage.getItem('user'));

  }
  isAuth() {
    return this.afAuth.authState.pipe(map(auth => auth));
  }

  async login(email: string, password: string) {
    await this.afAuth.auth.signInWithEmailAndPassword(email, password);



  }
  async register(email: string, password: string) {
    var result = await this.afAuth.auth.createUserWithEmailAndPassword(email, password)
    this.sendEmailVerification();
  }
  async sendEmailVerification() {
    await this.afAuth.auth.currentUser.sendEmailVerification().then(function () {
    }).catch(function (error) {
    });
    this.crearUser();
    localStorage.setItem("logueado","false");
    this.router.navigate(['']);
  }
  


  async sendPasswordResetEmail(passwordResetEmail: string) {
    return await this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail);
  }
  async logout() {
    await this.afAuth.auth.signOut();
    localStorage.removeItem('user');
    localStorage.setItem("logueado","false");
  }
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null;
  }
  //para google no creo que lo utilice
  async  loginWithGoogle() {
    await this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
    this.router.navigate(['admin/list']);
  }
  getUsuario() {
    return this.user;
  }

  public generarID(){
    return this.firestore.createId();
  }



  public crearUser() {
    // crear usuario con sus datos
    let data = {
      Email: this.user.email,
      RutinaActual: "",
      Meta: "",
    }
    this.firestore.collection('user').doc(this.user.uid).set(data);
  }
  getDataUser() {
    return this.firestore.collection('user').doc(this.usuario.uid).snapshotChanges();
  }
  // para actualizar por si acaso tiene ya creado 
  public cambiarRutinaActual(idRutinaActual) {
    let data = {
      RutinaActual: idRutinaActual,
    }
    this.firestore.collection('user').doc(this.user.uid).update(data);
  }

public cambiarMeta(meta){
  let data={
    Meta:meta
  }
  this.firestore.collection('user').doc(this.user.uid).update(data);
}
  // Ejercicios
  public addEjercicios(datos) {
   let data={
    Nombre: datos.nombre,
    Tipo: datos.musculo,
    Description: datos.descripcion,
    Hist:[],
    Ocultar:false,
   }
   let _id=this.firestore.createId();


   this.firestore.collection('user').doc(this.user.uid).collection('Ejercicios').doc(_id).set(data);
   return _id;
  }
  public getRefEjercicio(id){
    return this.firestore.collection('user').doc(this.user.uid).collection('Ejercicios').doc(id).ref;
  }
  public getEjercicios(){
    return this.firestore.collection('user').doc(this.user.uid).collection('Ejercicios').snapshotChanges();
  }
  public getEjercicio(id){
    return this.firestore.collection('user').doc(this.user.uid).collection('Ejercicios').doc<Ejer>(id).snapshotChanges();
  }
  public setEjercicios(id,data){
    return this.firestore.collection('user').doc(this.user.uid).collection('Ejercicios').doc(id).update(data);
  }
  public ocultarEjercicio(id){
      let data={
        Ocultar:true,
      }
      return this.firestore.collection('user').doc(this.user.uid).collection('Ejercicios').doc(id).update(data);
  
  }
  public addHitorialEjercicio(idEjercicio, data){

    this.firestore.collection('user').doc(this.user.uid).collection('Ejercicios').doc(idEjercicio).update({Hist:firestore.FieldValue.arrayUnion(data)});
  }


  //Rutinas
  public crearRutina(datos){
    let date = new Date(datos.fecha);
    let data = {
      Nombre:datos.nombre,
      DiasEntrenar: datos.diasEntrenamiento,
      DuracionSemanas: datos.duracionSemanas,
      FechaInicio: firestore.Timestamp.fromDate(date),
    }
    let a = this.firestore.createId();
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(a).set(data);
    return a;
    
  }
  public crearDia(numero,id){
    let _id= this.firestore.createId();
    let titulo="Día "+numero
    let data={
      Titulo:titulo,
      Numero:1,
    }
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(id).collection('dias').doc(_id).set(data);
    return _id;
    }
  public updateDia( idRutina,idDia, data){
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).collection('dias').doc(idDia).update(data);
  }
  public addEjerDia(idRutina,idDia,data,data1){
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).collection('dias').doc(idDia).update({Vector:firestore.FieldValue.arrayUnion(data1)});
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).collection('dias').doc(idDia).update({Ejercicios:firestore.FieldValue.arrayUnion(data)});
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).collection('dias').doc(idDia).update({Numero:firestore.FieldValue.increment(1)});
  }
  public updateRutinaAddNumDias(idRutina,n){
    let data={
      DiasEntrenar:""+n,
    }
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).update(data);
  }
  public updateRutinaQuitarNumDias(idRutina,n){
    let data={
      DiasEntrenar:""+n,
    }
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).update(data);
  }

  /**
   * Funcio para eliminar el ejercicio pasando exaxtamente los mismo datos;
   * @param idDia 
   * @param idRutina 
   * @param data 
   */
  public deleteEjerRutina(idDia,idRutina,data,data1){
    //se le pasan datos iguales
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).collection('dias').doc(idDia).update({Vector:firestore.FieldValue.arrayRemove(data1)})
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).collection('dias').doc(idDia).update({Ejercicios:firestore.FieldValue.arrayRemove(data)});
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).collection('dias').doc(idDia).update({Numero:firestore.FieldValue.increment(-1)});
  }
  public deleteDia( idRutina,idDia){
   
    this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).collection('dias').doc(idDia).delete();
  }

  //HISTORIAL
  public getHirtorial(){
    return this.firestore.collection('user').doc(this.user.uid).collection('Historial').snapshotChanges();

  }
  public addHistorial(data){
    
    let datos= {
      Duracion: data.duracion,
      dia: data.dia,
      idRutina: data.idRutina,
      nombreRutina:data.nombreRutina,
      fecha: firestore.Timestamp.fromDate(data.fecha),
    }
     this.firestore.collection('user').doc(this.user.uid).collection('Historial').add(datos);
  }

  //PESOS
  public getHitorialPesos(){
   return  this.firestore.collection('user').doc(this.user.uid).collection('Pesos').snapshotChanges()
  }
  public addHistorialPeso(data){
    let _data={
      IdHistorial:data.id,
      Fecha:firestore.Timestamp.fromDate(data.fecha),
      Peso:data.peso,
    }
    this.firestore.collection('user').doc(this.user.uid).collection('Pesos').add(_data);
  }
  public addActualizacionPeso(data){
    let datos= {
      Duracion: ""+data.peso,
      dia: "Actualización Peso",
      idRutina:"",
      nombreRutina:"",
      fecha: firestore.Timestamp.fromDate(data.fecha),
    }
     this.firestore.collection('user').doc(this.user.uid).collection('Historial').doc(data.id).set(datos);
  }
  public deletePeso(idPeso){
    this.firestore.collection('user').doc(this.user.uid).collection('Pesos').doc(idPeso).delete();
  }
  public deleteHistorial(idHistorial){
    this.firestore.collection('user').doc(this.user.uid).collection('Historial').doc(idHistorial).delete();
  }

  public getRutina(idRutina) {
    return this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).collection('dias').snapshotChanges();
  }
  public deleteRutina(idRutina){
    return this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).delete();
  }
  public getDatosRutina(idRutina){
  return this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc<Rutina>(idRutina).snapshotChanges();
    
  }
  //Funcion para consultar un dia especifico;
  public getRutinaDia(idDia, idRutina) {
    return this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').doc(idRutina).collection('dias').doc<DIA>(idDia).snapshotChanges();
  }
  public getRutinas(){
    return this.firestore.collection('user').doc(this.usuario.uid).collection('Rutinas').snapshotChanges();
  }

}