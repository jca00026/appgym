import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ComunicacionService {
  private titulo = new BehaviorSubject<string>('Rutina Actual');
  public custommessage =  this.titulo.asObservable();
  private idRutina='';
  
  private idDiaRutina='';
  private mesCalendarSource = new BehaviorSubject<string>('nada');
  dateMes = this.mesCalendarSource.asObservable();
  constructor() {
    
   }
  public setTitulo(titulo: string){
    this.titulo.next(titulo);
    localStorage.setItem("titulo",titulo);
    
  }
  public getTitulo(){ 
    return this.titulo;
  }
  public getidRutina(){
    return localStorage.getItem("idRutina");
  }
  public getIdDiaRutina(){
    return localStorage.getItem("idDiaRutina");
    
  }
  public setIdDiaRutina(id:string){
    localStorage.setItem("idDiaRutina",id);
  }
  public setIdRutina(id:string){
    localStorage.setItem("idRutina",id);
  }
public cambiaMes(message: string) {
  this.mesCalendarSource.next(message);
}



}
