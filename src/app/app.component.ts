import { Component } from '@angular/core';
import { ComunicacionService } from './servicio/comunicacion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private comucicaionService: ComunicacionService){
    let titulo="Rutina Actual";
    comucicaionService.setTitulo(titulo);
    
  }
  isLoggedIn() {
    return localStorage.getItem('isLoggedIn') === 'true';
  }
  
}
